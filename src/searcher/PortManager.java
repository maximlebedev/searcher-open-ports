package searcher;

import java.net.InetAddress;
import java.net.UnknownHostException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CyclicBarrier;

/**
 * Класс заполнения "поисковиков"
 */
public class PortManager {

    public static final int MIN_PORTS_PER_THREAD = 20;
    public static final int MAX_THREADS = 0xFF;

    static InetAddress inetAddress;
    static List<Integer> allPorts;

    private String ip;
    private int minPort;
    private int maxPort;

    public PortManager(String ip, int minPort, int maxPort) {
        this.ip = ip;
        this.minPort = minPort;
        this.maxPort = maxPort;
    }

    /**
     * Метод создания объектов "поисковиков", запуск
     * @return список найденных открытых портов
     * @throws UnknownHostException Исключение связанное с не определением IP адреса
     */
    public List<Integer> findOpenPorts() throws UnknownHostException {

        inetAddress = InetAddress.getByName(ip);

        allPorts = new ArrayList<>(maxPort - minPort + 1);

        for (int i = minPort; i <= maxPort; i++) {
            allPorts.add(i);
        }

        List<PortSearcher> searchers = new ArrayList<>(MAX_THREADS);

        if (allPorts.size() / MIN_PORTS_PER_THREAD > MAX_THREADS) {
            final int PORTS_PER_THREAD = allPorts.size() / MAX_THREADS;

            List<Integer> threadPorts = new ArrayList<>();
            for (int i = 0, counter = 0; i < allPorts.size(); i++, counter++) {
                if (counter < PORTS_PER_THREAD) {
                    threadPorts.add(allPorts.get(i));
                } else {
                    PortSearcher portSearcher = new PortSearcher();
                    portSearcher.setInetAddress(inetAddress);
                    portSearcher.setPorts(new ArrayList<>(threadPorts));
                    searchers.add(portSearcher);
                    threadPorts.clear();
                    counter = 0;
                }
            }
            PortSearcher portSearcher = new PortSearcher();
            portSearcher.setInetAddress(inetAddress);
            portSearcher.setPorts(new ArrayList<>(threadPorts));
            searchers.add(portSearcher);
        } else {
            List<Integer> threadPorts = new ArrayList<>();
            for (int i = 0, counter = 0; i < allPorts.size(); i++, counter++) {
                if (counter < MIN_PORTS_PER_THREAD) {
                    threadPorts.add(allPorts.get(i));
                } else {
                    PortSearcher portSearcher = new PortSearcher();
                    portSearcher.setInetAddress(inetAddress);
                    portSearcher.setPorts(new ArrayList<>(threadPorts));
                    searchers.add(portSearcher);
                    threadPorts.clear();
                    counter = 0;
                }
            }
            PortSearcher portSearcher = new PortSearcher();
            portSearcher.setInetAddress(inetAddress);
            portSearcher.setPorts(new ArrayList<>(threadPorts));
            searchers.add(portSearcher);
        }

        List<Integer> allOpenPorts = new ArrayList<>();

        Runnable summarizer = () -> {

            for (PortSearcher portSearcher : searchers) {
                List<Integer> openPorts = portSearcher.getOpenPorts();
                allOpenPorts.addAll(openPorts);
            }

            Collections.sort(allOpenPorts);
        };

        CyclicBarrier barrier = new CyclicBarrier(searchers.size(), summarizer);

        for (PortSearcher portSearcher : searchers) {
            portSearcher.setBarrier(barrier);
        }

        List<Thread> threads = new ArrayList<>();

        for (PortSearcher portSearcher : searchers) {
            Thread thread = new Thread(portSearcher);
            threads.add(thread);
            thread.start();
        }

        for (Thread thread : threads) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        return allOpenPorts;
    }
}