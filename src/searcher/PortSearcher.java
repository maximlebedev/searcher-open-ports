package searcher;

import java.net.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.io.IOException;

/**
 * Класс "поисковиков" портов
 * @author Лебедев М.Д.
 */
public class PortSearcher implements Runnable {

    private List<Integer> ports;
    private List<Integer> openPorts;
    private InetAddress inetAddress;
    private int timeout = 200;
    CyclicBarrier barrier;

    /**
     * Метод устанавливает барьер для потоков
     * @param barrier барьер
     */
    public void setBarrier(CyclicBarrier barrier) {
        this.barrier = barrier;
    }

    /**
     * Возвращает список открытых портов
     * @return список портов
     */
    public List<Integer> getOpenPorts() {
        return openPorts;
    }

    /**
     * Метод устанавливает IP адрес по которому нужно искать
     * @param inetAddress IP
     */
    public void setInetAddress(InetAddress inetAddress) {
        this.inetAddress = inetAddress;
    }

    /**
     * Метод устанавливает время подключения
     * @param timeout время
     */
    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    /**
     * Метод устанавливает порты по которым нужно искать
     * @param ports список порты
     */
    public void setPorts(List<Integer> ports) {
        this.ports = ports;
    }

    public void run() {
        scan(inetAddress);
        try {
            barrier.await();
        } catch (InterruptedException | BrokenBarrierException ignored) {
        }
    }

    /**
     * Метод проводит подключения к портам к конкретному IP, с определенным временем для подключением
     * @param inetAddress IP
     */
    void scan(InetAddress inetAddress) {
        openPorts = new ArrayList<>();
        for (Integer port : ports) {
            try {
                InetSocketAddress isa = new InetSocketAddress(inetAddress, port);
                Socket socket = new Socket();
                socket.connect(isa, timeout);
                openPorts.add(port);
                socket.close();
            } catch (IOException ignored) {
            }
        }
    }
}