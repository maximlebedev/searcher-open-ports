package sample.controllers;

import database.DataBase;
import searcher.PortManager;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.*;

import java.net.UnknownHostException;
import java.sql.SQLException;

import java.util.List;
import java.util.regex.Pattern;

/**
 * Класс контроллера главного экрана
 * @author Лебедев М.Д.
 */
public class Controller {

    @FXML
    private TextField ip;
    @FXML
    private TextField minPort;
    @FXML
    private TextField maxPort;
    @FXML
    private Label errorField;
    @FXML
    private TextArea portsArea;
    @FXML
    private Button buttonSearch;
    @FXML
    private Button buttonJournal;

    final private String REGEX_IP = "(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)";

    /**
     * Метод который вызывается во время нажатия на кнопку "Поиск" совершает поиск по значениям в полях
     */
    @FXML
    public void search() {

        checkForEmptiness();

        Runnable search = () -> {
            setDisableForButton(true);
            portsArea.setText("Производится поиск по ip: " + ip.getText());

            PortManager portManager = new PortManager(ip.getText(), Integer.parseInt(minPort.getText()), Integer.parseInt(maxPort.getText()));

            try {
                List<Integer> portsList = portManager.findOpenPorts();

                if (portsList.toString().equals("[]")) {
                    portsArea.setText(portsArea.getText() + "\nДля данного IP не найдены порты");
                } else {
                    portsArea.setText(portsArea.getText() + "\nСписок открытых портов:\n" + portsList.toString());

                    Platform.runLater(
                            () -> {
                                DataBase dataBase = new DataBase();
                                try {
                                    dataBase.insert(ip.getText(), portsList.toString());
                                } catch (SQLException | NullPointerException e) {
                                    buttonJournal.setDisable(true);
                                    giveError("Не удалось записать данные в БД.");
                                }
                            }
                    );
                }

                setDisableForButton(false);
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }
        };

        try {
            if (checkIP(ip.getText()) && checkPorts(Integer.parseInt(minPort.getText()), Integer.parseInt(maxPort.getText()))) {
                if (!errorField.getText().equals("")) {
                    errorField.setText("");
                }
                new Thread(search).start();
            }
        } catch (NumberFormatException e) {
            giveError("Неверный тип вводимых данных у порта.");
        }
    }

    /**
     * Метод который вызывается во время нажатия на кнопку "Журнал" и открывает новое окно с записями в БД
     */
    @FXML
    public void showJournal() {

        ControllerJournal controllerJournal = new ControllerJournal();
        try {
            controllerJournal.createWindow();
        } catch (SQLException | NullPointerException e) {
            buttonJournal.setDisable(true);
            giveError("Не удалось подключится к БД.");
        }
    }

    /**
     * Метод проверяет поля на пустоту, и в случае пустоты заполняет их стандартными значениями
     */
    public void checkForEmptiness() {

        if (ip.getText().equals("")) {
            ip.setText("localhost");
        }

        if (minPort.getText().equals("")) {
            minPort.setText("0");
        }

        if (maxPort.getText().equals("")) {
            maxPort.setText("65535");
        }
    }

    private boolean checkIP(String ip) {

        if (Pattern.matches(REGEX_IP, ip) | ip.equals("localhost")) {
            return true;
        } else {
            giveError("Некорректный IP.");
        }

        return false;
    }

    private boolean checkPorts(int minPort, int maxPort) {

        if (minPort > maxPort) {
            giveError("Порт с которого начинается поиск больше, чем тот по какой искать.");
        } else if (minPort < 0 | minPort > 65535) {
            giveError("Некорректно введён порт с которого нужно искать.");
        } else if (maxPort > 65535) {
            giveError("Некорректно введёт порт по какой нужно искать.");
        } else {
            return true;
        }

        return false;
    }

    /**
     * Метод выдачи ошибки пользователю
     * @param message сообщение ошибки
     */
    public void giveError(String message) {

        errorField.setText(message);
    }

    private void setDisableForButton(boolean value) {

        buttonSearch.setDisable(value);
        ip.setDisable(value);
        minPort.setDisable(value);
        maxPort.setDisable(value);
    }
}
