package sample.controllers;

import database.DataBase;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;

import java.sql.SQLException;
import java.util.List;

/**
 * Класс контроллера окна журнала
 * @author Лебедев М.Д.
 */
public class ControllerJournal {

    public void createWindow() throws SQLException {
        Stage stage = new Stage();

        DataBase dataBase = new DataBase();
        List<String> result = dataBase.select();
        ObservableList<String> log = FXCollections.observableArrayList(result);
        ListView<String> logListView = new ListView<>(log);
        logListView.setPrefWidth(600);
        logListView.setPrefHeight(200);
        FlowPane root = new FlowPane(logListView);
        Scene scene = new Scene(root);

        stage.setScene(scene);
        stage.setTitle("Journal");
        stage.show();
    }
}
