package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        Parent parent = FXMLLoader.load(getClass().getResource("views/Interface.fxml"));
        stage.setScene(new Scene(parent, 600, 400));
        stage.setTitle("Поисковик открытых портов");
        stage.show();
        stage.setMinWidth(500);
        stage.setMinHeight(400);
        stage.setMaxWidth(650);
        stage.setMaxHeight(450);
    }
}