package database;

import java.sql.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;

/**
 * Класс для работы с базой данных
 * @author Лебедев М.Д.
 */
public class DataBase implements MySQLDataBaseConfig {

    private static Connection connection;

    static {
        try {
            connection = getConnection();
        } catch (Exception ignored) {
        }
    }

    private static Connection getConnection() throws Exception {
        Connection connection;

        Class.forName(DRIVER_NAME).getDeclaredConstructor().newInstance();
        connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);

        return connection;
    }

    /**
     * Метод добавляет в таблицу list_ports данные: текущее время, ip, список открытых порты
     * @param ip ip
     * @param ports порты
     * @throws SQLException Исключение, которое возникает, когда SQL Server возвращает предупреждение или ошибку.
     */
    public void insert(String ip, String ports) throws SQLException {
        Date date = new Date();
        String currentDate = date.getYear() + "-" + date.getMonth() + "-" + date.getDate();

        String query = "INSERT INTO list_ports (date_request, ip, ports) VALUES (" +
                "\"" + currentDate + "\", " +
                "\"" + ip + "\", " +
                "\"" + ports + "\");";

        Statement statement = connection.createStatement();
        statement.executeUpdate(query);
    }

    /**
     * Метод делает запрос к БД и получает список всех данных в таблице list_ports
     * @return массив с данными из таблицы list_ports
     * @throws SQLException Исключение, которое возникает, когда SQL Server возвращает предупреждение или ошибку.
     */
    public List<String> select() throws SQLException {
        List<String> result = new ArrayList<>();
        String query = "SELECT * FROM list_ports;";
        ResultSet resultSet = connection.createStatement().executeQuery(query);

        while (resultSet.next()) {
            String data = "ID:" + resultSet.getInt("id") + " / " +
                    "Date: " + resultSet.getString("date_request") + " / " +
                    "IP: " + resultSet.getString("ip") + " / " +
                    "Ports: " + resultSet.getString("ports") + ";";
            result.add(data);
        }

        return result;
    }

    /**
     * Метод закрывает подключение с БД
     * @throws SQLException Исключение, которое возникает, когда SQL Server возвращает предупреждение или ошибку.
     */
    public void closeConnection() throws SQLException {
        connection.close();
    }
}
