package database;

/**
 * Интерфейс для подключения к БД
 * @author Лебедев М.Д.
 */
public interface MySQLDataBaseConfig {

    String URL = "jdbc:mysql://localhost:3306/ports?serverTimezone=Europe/Moscow&useSSL=false";
    String USERNAME = "root";
    String PASSWORD = "";
    String DRIVER_NAME = "com.mysql.cj.jdbc.Driver";
}
