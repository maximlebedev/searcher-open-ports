package sample.controllers;

import org.junit.Test;

import java.sql.SQLException;


public class ControllerTest {

    @Test(expected = ExceptionInInitializerError.class)
    public void checkShowJournal() throws SQLException {
        ControllerJournal controllerJournal = new ControllerJournal();

        controllerJournal.createWindow();
    }
}