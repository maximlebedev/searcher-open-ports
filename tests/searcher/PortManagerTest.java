package searcher;

import org.junit.Test;

import java.net.UnknownHostException;


public class PortManagerTest {

    @Test(expected = UnknownHostException.class)
    public void validateIP() throws UnknownHostException {
        String ip = "213.431.231.5";

        PortManager portManager = new PortManager(ip, 0, 100);
        portManager.findOpenPorts();
    }
}