package searcher;

import org.junit.Test;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;

public class PortSearcherTest {

    @Test(expected = IllegalArgumentException.class)
    public void run() throws UnknownHostException {
        ArrayList<Integer> ports = new ArrayList<>();
        for (int i = -10; i < 0; i++) {
            ports.add(i);
        }
        PortSearcher portSearcher = new PortSearcher();
        portSearcher.setPorts(ports);
        portSearcher.setTimeout(200);

        portSearcher.scan(InetAddress.getByName("localhost"));
    }
}